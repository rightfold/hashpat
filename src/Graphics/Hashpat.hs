{-# LANGUAGE OverloadedStrings #-}
module Graphics.Hashpat
( hashpat
) where

import Control.Monad (replicateM)
import Control.Monad.Random (evalRand, getRandomR, MonadRandom, uniform)
import Data.Monoid ((<>))
import System.Random (mkStdGen)
import Text.Blaze.Svg11 ((!), Markup, Svg)
import qualified Text.Blaze.Svg11 as S
import qualified Text.Blaze.Svg11.Attributes as A

type Color = (Int, Int, Int, Double)

lighten :: Int -> Color -> Color
lighten n (h, s, l, a) = (h, s, l + n, a)

darken :: Int -> Color -> Color
darken n c = lighten (negate n) c

transparentize :: Double -> Color -> Color
transparentize t (h, s, l, a) = (h, s, l, a - t)

svgColor :: Color -> S.AttributeValue
svgColor (h, s, l, a) = S.toValue hsla
  where hsla = "hsla(" ++ show h ++ ", " ++ show s ++ "%, " ++ show l ++ "%, " ++ show a ++ ")"

svgDouble :: Double -> S.AttributeValue
svgDouble = S.toValue . show

svgRect :: Double -> Double -> Double -> Double -> Color -> S.Markup
svgRect x y w h c = S.rect ! A.x (svgDouble x)
                           ! A.y (svgDouble y)
                           ! A.width (svgDouble w)
                           ! A.height (svgDouble h)
                           ! A.fill (svgColor c)

hashpat :: (Double, Double) -> Int -> Svg
hashpat (w, h) = evalRand (hashpat' (w, h)) . mkStdGen

hashpat' :: MonadRandom m => (Double, Double) -> m Svg
hashpat' (w, h) = do
  primaryColor <- getRandomColor
  secondaryColor <- (\t -> transparentize t $ darken 10 $ primaryColor)
                      <$> getRandomR (0.0, 0.1)

  let background = S.rect ! A.x (svgDouble 0.0)
                          ! A.y (svgDouble 0.0)
                          ! A.width (svgDouble w)
                          ! A.height (svgDouble h)
                          ! A.fill (svgColor primaryColor)
  pattern <- getRandomPattern (w, h) secondaryColor

  return $ S.docTypeSvg ! A.version "1.1"
                        ! A.width (svgDouble w)
                        ! A.height (svgDouble h)
                        $ background >> pattern

getRandomColor :: MonadRandom m => m Color
getRandomColor = (,,,) <$> getRandomR (0, 359)
                       <*> getRandomR (60, 80)
                       <*> getRandomR (40, 60)
                       <*> return 1.0

getRandomPattern :: MonadRandom m => (Double, Double) -> Color -> m Markup
getRandomPattern (w, h) color = uniform =<< sequence patterns
  where patterns = [checkerBoard, squares, circles, linnen]

        checkerBoard = do
          let points = [(x, y) | x <- [0..15], y <- [0..15]]
          (mconcat <$>) $ flip mapM points $ \(x, y) -> do
            c <- (\f -> darken (round f) color) <$> getRandomR (0.5 :: Double, 15.0)
            return $ svgRect (fromIntegral x * w / 16.0)
                             (fromIntegral y * h / 16.0)
                             (w / 16.0)
                             (h / 16.0)
                             c

        squares = do
          let unit = (w + h) / 2.0 / 80.0
          let points = [(x, y, n) | x <- [0..7], y <- [0..7], n <- [0.0..3.0]]
          (mconcat <$>) $ flip mapM points $ \(x, y, n) -> do
            c <- (\f -> darken (round f) color) <$> getRandomR (0.5 :: Double, 15.0)
            return $ svgRect (fromIntegral x * w / 8.0 + unit * n)
                             (fromIntegral y * h / 8.0 + unit * n)
                             (w / 8.0 - unit * n * 2)
                             (h / 8.0 - unit * n * 2)
                             c

        circles = do
          n <- round <$> getRandomR (w / 50.0, w / 70.0)
          (mconcat <$>) . replicateM n $ do
            x <- getRandomR (0.0, w - 1.0)
            y <- getRandomR (0.0, h - 1.0)
            r <- getRandomR ((w + h / 2.0) / 10, (w + h / 2.0) / 5)
            w <- getRandomR ((w + h / 2.0) / 35, (w + h / 2.0) / 20)
            return $ S.circle ! A.cx (svgDouble x)
                              ! A.cy (svgDouble y)
                              ! A.r (svgDouble r)
                              ! A.fill "none"
                              ! A.stroke (svgColor color)
                              ! A.strokeWidth (svgDouble w)

        linnen = (<>) <$> (mconcat <$> linnen' 0) <*> (mconcat <$> linnen'' 0)
        linnen' x | x > w     = return []
                  | otherwise = do
                      sw <- getRandomR (w / 50, w / 40)
                      let sc = if sw < w / 48.0 then darken 20 color else color
                      let rect = svgRect x 0.0 sw h sc
                      (rect :) <$> linnen' (x + sw - w / 250.0)
        linnen'' y | y > h     = return []
                   | otherwise = do
                       sw <- getRandomR (h / 50, h / 40)
                       rects <- if not (sw < w / 48.0) then return [] else do
                         let sc = darken 20 color
                         let rect = svgRect 0.0 y w sw sc
                         return [rect]
                       (rects ++) <$> linnen'' (y + sw - h / 250.0)
