module Main (main) where

import Graphics.Hashpat (hashpat)
import System.Random (randomIO)
import Text.Blaze.Svg.Renderer.String (renderSvg)

main :: IO ()
main = do
  seed <- randomIO
  mapM_ (putStrLn . renderSvg . hashpat (320.0, 320.0) . (seed +)) [1..21]
